const CONFIG = {
    CLIENT_ID: '2',
    CLIENT_SECRET: 'iJc1MMLSFrm3t7LCjXPI2V5G76AivIyMakT8SNVQ',
    API_VERSION: 'v1',
    API_URL: '/api',
    TABLES: {
        striped: true,
        bordered: true,
        hover: true,
        dark: false,
        footClone: true,
        headerStyle: 'light',
        captions: true,
        responsive: 'xl'
    }
};
export default CONFIG;