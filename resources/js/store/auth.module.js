import ApiService from '../services/ApiService';
import JwtService from '../services/JwtService';
import AuthService from "../services/AuthService";

const state = {
    error: null,
    user: {},
    isAuthenticated: !!JwtService.getToken(),
    permissions: []
};

const getters = {
  currentUser (state) {
    return state.user;
  },
  isAuthenticated (state) {
    return state.isAuthenticated;
  },
  permissions (state) {
      return state.permissions;
  }
};

const actions = {
    login (context, credentials) {
        return AuthService.login(credentials);
    },
    logout (context) {
        context.commit('purgeAuth');
    },
    checkAuth(context, sectionName) {
        if (typeof state.user !== 'undefined' && typeof state.user.id !== 'undefined') {
            return true;
        } else if (JwtService.getToken()) {
            ApiService.init();
            ApiService.setHeader();
            return ApiService.query('user', {params : {section: sectionName}})
                .then(({ data }) => {
                    context.commit('setAuth', data)
                })
                .catch(({ response }) => {
                    context.commit('purgeAuth')
                    if (response && response.error) {
                        context.commit('setError', response.error)
                    }
                });
        } else {
            context.commit('purgeAuth')
        }
    }
};

const mutations = {
    setError(state, error) {
        state.error = error;
    },
    setToken(state, data) {
        if (state.isAuthenticated = data.access_token !== 'undefined') {
            JwtService.saveToken(data.access_token);
        }
        state.error = {};
    },
    setAuth(state, data) {
        state.user = data;
    },
    purgeAuth(state) {
        state.isAuthenticated = false;
        state.user = {};
        state.error = {};
        JwtService.destroyToken();
    }
};

export default {
    state,
    actions,
    mutations,
    getters
};
