import ApiService from './ApiService';
import config from "../config";

const AuthService = {
	login(credentials) {
		ApiService.init();
		return axios.post('oauth/token', {
			grant_type: 'password',
            client_id: config.CLIENT_ID,
            client_secret: config.CLIENT_SECRET,
            username: credentials.email,
            password: credentials.password,
            scope: '*'
		})
	},
};

export default AuthService;