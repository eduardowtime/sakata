import Vue from 'vue';
import axios from 'axios';
import VueAxios from 'vue-axios';
import config from "../config";
import JwtService from './JwtService';

const ApiService = {
  init() {
    Vue.use(VueAxios, axios);
    Vue.axios.defaults.baseURL = config.API_URL;
    if (JwtService.getToken()) {
        this.setHeader();
    }
    Vue.axios.interceptors.response.use((response) => {
        return response
    }, function (error) {
        let originalRequest = error.config;
        if (
            error.response.status === 401
            && JwtService.getToken()
            && !originalRequest._retry
            && !/oauth/.test(originalRequest.url)
        ) {
            return Vue.axios.post('oauth/token/refresh', null).then((response) => {
                JwtService.saveToken(response.data.access_token);
                Vue.axios.defaults.headers.common['Authorization'] = `Bearer ${JwtService.getToken()}`;
                originalRequest.headers['Authorization'] = `Bearer ${JwtService.getToken()}`;
                originalRequest.url = originalRequest.url.toString().replace(originalRequest.baseURL, '');
                return Vue.axios(originalRequest);
            }).catch((error) => {
                return;
            })
        }
        return Promise.reject(error);
    });
  },
  setLanguageHeader(lang) {
    Vue.axios.defaults.headers.common['Accept-Language'] = lang;
  },
  setHeader() {
    Vue.axios.defaults.headers.common['Authorization'] = `Bearer ${JwtService.getToken()}`;
  },

  query(resource, params) {
    return Vue.axios.get(resource, params = {}).catch(error => {
      throw new Error(`ApiService ${error}`);
    });
  },

  options(resource, params) {
      return Vue.axios.options(resource, params).catch(error => {
          throw new Error(`ApiService ${error}`);
      });
  },

  get(resource, slug = '') {
    return Vue.axios.get(`${resource}/${slug}`).catch(error => {
      throw new Error(`ApiService ${error}`);
    });
  },

  post(resource, params) {
    return Vue.axios.post(`${resource}`, params);
  },

  update(resource, slug, params) {
    return Vue.axios.put(`${resource}/${slug}`, params);
  },

  upload(resource, params) {
    return Vue.axios.post(`${resource}`, params, 
    {
      headers: {
          'Content-type': 'multipart/form-data'
      }
    });
  },

  put(resource, params) {
    return Vue.axios.put(`${resource}`, params);
  },

  delete(resource, slug) {
    return Vue.axios.delete(`${resource}/${slug}`).catch(error => {
      throw new Error(`ApiService ${error}`);
    });
  }
};

export default ApiService;
