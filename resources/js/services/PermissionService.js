import auth from '@/store/auth.module';

const PermissionService = {
	hasPermission(permission) {
		if(auth.state.user.permissions != null){
			return (auth.state.user.permissions.indexOf(permission) >= 0)
		} 
		return false;
	}
}

export default PermissionService;