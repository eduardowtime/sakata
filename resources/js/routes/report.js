export default [{
    path: '/report',
    component: () => import('../components/Report/Module'),
    children: [{
        path: 'solicitations',
        component: () => import('../components/Report/Solicitation'),
        name: 'Relatório de solicitações'
    }]
}]