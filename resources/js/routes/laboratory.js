export default [{
    path: '/labs',
    component: () => import('../components/Laboratory/Module'),
    children: [{
        path: '',
        component: () => import('../components/Laboratory/List'),
        name: 'Laboratórios'
    }, {
        path: 'new',
        component: () => import('../components/Laboratory/Form'),
        name: 'Cadastrar laboratório'
    }]
}]
