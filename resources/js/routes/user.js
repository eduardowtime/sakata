export default [{
    path: '/users',
    component: () => import('../components/User/Module'),
    children: [{
        path: '',
        component: () => import('../components/User/List'),
        name: 'Usuários'
    }, {
        path: 'new',
        component: () => import('../components/User/Form'),
        name: 'Cadastro de usuários'
    }, {
        path: ':id',
        component: () => import('../components/User/Form'),
        name: 'Editar usuário'
    }]
}]
