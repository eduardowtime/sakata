export default [{
    path: '/tools',
    component: () => import('../components/Tools/Module'),
    children: [{
        path: '',
        component: () => import('../components/Tools/List'),
        name: 'Ferramentas'
    }, {
        path: 'new',
        component: () => import('../components/Tools/Form'),
        name: 'Cadastrar ferramentas'
    }]
}]
