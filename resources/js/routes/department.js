export default [{
    path: '/depts',
    component: () => import('../components/Department/Module'),
    children: [{
        path: '',
        component: () => import('../components/Department/List'),
        name: 'Departamentos'
    }, {
        path: 'new',
        name: 'Novo departamento',
        component: () => import('../components/Department/Form'),
    }]
}]
