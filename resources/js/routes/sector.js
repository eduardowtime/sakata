export default [{
    path: '/sectors',
    component: () => import('../components/Sector/Module'),
    children: [{
        path: '',
        component: () => import('../components/Sector/List'),
        name: 'Setores'
    }, {
        path: 'new',
        name: 'Novo setor',
        component: () => import('../components/Sector/Form'),
    }]
}]
