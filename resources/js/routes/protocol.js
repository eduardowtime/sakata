export default [{
    path: '/protocols',
    component: () => import('../components/Protocol/Module'),
    children: [{
        path: '',
        component: () => import('../components/Protocol/List'),
        name: 'Protocolo'
    }, {
        path: 'new',
        component: () => import('../components/Protocol/Form'),
        name: 'Cadastrar protocolo'
    }, {
        path: ':id/procedure',
        component: () => import('../components/Protocol/Procedure'),
        name: 'Cadastrar Procedimento'
    }
    , {
        path: ':id/activities',
        component: () => import('../components/Protocol/Activities'),
        name: 'Cadastrar Atividades'
    }
    , {
        path: ':id/description',
        component: () => import('../components/Protocol/Description'),
        name: 'Cadastrar Descrição'
    }
    , {
        path: ':id/view',
        component: () => import('../components/Protocol/View'),
        name: 'Visualizar documento'
    }]
}]