export default [{
    path: '/solicitations',
    component: () => import('../components/Solicitations/Module'),
    children: [{
        path: '',
        component: () => import('../components/Solicitations/List'),
        name: 'Solicitações',
    }, {
        path: 'new',
        component: () => import('../components/Solicitations/Form'),
        name: 'Nova solicitação'
    }, {
        path: ':id/view',
        component: () => import('../components/Solicitations/View'),
        name: 'Dados da solicitação'
    }, {
        path: ':id/items',
        component: () => import('../components/Solicitations/Item'),
        name: 'Itens da solicitação'
    }, {
        path: ':id/protocol',
        component: () => import('../components/Solicitations/Protocol'),
        name: 'Protocolo da solicitação'
    }, {
        path: ':id/abstract',
        component: () => import('../components/Solicitations/Abstract'),
        name: 'Abstract'
    }, {
        path: ':id/attachments',
        component: () => import('../components/Solicitations/Attachment'),
        name: 'Anexos'
    }, {
        path: ':id/croqui',
        component: () => import('../components/Solicitations/Croqui'),
        name: 'Croqui'
    }, {
        path: ':id/result',
        component: () => import('../components/Solicitations/Result'),
        name: 'Resultado'
    }]
}]
