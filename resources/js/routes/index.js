import Login from '../layout/Login';
import Layout from '../layout/Layout';
import Print from '../layout/Print';
import solicitationsRoutes from './solicitations';
import usersRoutes from './user';
import labsRoutes from './laboratory';
import deptRoutes from './department';
import toolRoutes from './tool';
import sectorRoutes from './sector';
import protocolRoutes from './protocol';
import reportRoutes from './report';

const routes = [
    {
    path: '/login',
    component: Login,
        children: [{
            path: '',
            name: 'login',
            component: () => import('../components/Auth/AuthenticationLogin')
        }]
    }, {
    path: '/',
    component: Layout,
    meta: {
        requiresAuth: true
    },
    children: [{
        path: '',
        name: 'Dashboard',
        component: () => import('../components/Dashboard'),
    },
    {
        path: 'profile',
        name: 'Profile',
        component: () => import('../components/Profile'),
    }]
        .concat(solicitationsRoutes, 
                usersRoutes,
                labsRoutes,
                deptRoutes,
                toolRoutes,
                sectorRoutes,
                protocolRoutes,
                reportRoutes)
    }, {
        path: '/print/:id',
        component: Print,
        children: [{
            path: '',
            name: 'print',
            component: () => import('../components/Report/Print')
        }]
    }, {
        path: '/print2/:id',
        component: Print,
        children: [{
            path: '',
            name: 'print2',
            component: () => import('../components/Report/Print2')
        }]
    }]
export default routes