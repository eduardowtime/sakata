import Vue from 'vue'
import VueI18n from 'vue-i18n'


const selectLocale = process.env.VUE_APP_I18N_LOCALE || 'pt-BR';

Vue.use(VueI18n)

function loadLocaleMessages () {
  const locales = require.context('./lang', true, /[A-Za-z0-9-]\/[A-Za-z0-9-_,\s]+\.json$/i)
  const messages = {}
  locales.keys().forEach(key => {
    const matched = key.match(/([A-Za-z0-9-]+)\/([A-Za-z0-9-_]+)\./i)
    if (matched && matched.length > 2) {
      const locale = matched[1]
      const module = matched[2]
      messages[locale] = messages[locale] || []
      messages[locale][module] = locales(key)
    }
  })
  return messages
}

export default new VueI18n({
    locale: selectLocale,
    silentTranslationWarn: true,
    messages: loadLocaleMessages()
})