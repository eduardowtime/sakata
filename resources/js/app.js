require('./bootstrap');

import Vue from 'vue'
import VueRouter from 'vue-router'
import AppRoutes from './routes'
import { library } from '@fortawesome/fontawesome-svg-core';
import { FontAwesomeIcon } from "@fortawesome/vue-fontawesome";
import * as icons from './assets/fontawesome'
import "./assets/vee-validate";
import Buefy from "buefy";
import store from './store';
import Middleware from './middleware/UserHasPermissions';
import i18n from './i18n';
import Notifications from 'vue-notification'
import VueMoment from 'vue-moment'


Vue.use(VueRouter)
Vue.use(VueMoment)
Vue.use(Buefy,{
  defaultIconComponent: 'vue-fontawesome',
  defaultIconPack: 'fas',
});
Vue.use(Notifications)


Vue.component('vue-fontawesome', FontAwesomeIcon);

import App from './views/App'

const router = new VueRouter({
    mode: 'history',
    routes: AppRoutes
});

Middleware(router);

const app = new Vue({
    el: '#app',
    components: { App },
    router,
    store,
    i18n
});
