import store from '../store'

export default function UserHasPermissions (router) {
    router.beforeEach((to, from, next) => {
        let authorized = null
        Promise.all([store.dispatch('checkAuth', to.name)]).then(() => {
            if (to.matched.some(record => record.meta.requiresAuth)) {
                authorized = store.state.auth.isAuthenticated
                if (!authorized) {
                    next({
                        name: 'login',
                        query: {
                            redirect: to.fullPath
                        }
                    })
                }
            }
            next()
        }).catch(() => {
            next({
                name: 'login',
                query: {
                    redirect: to.fullPath
                }
            })
        });
    })
}