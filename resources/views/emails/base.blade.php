<!DOCTYPE html>
<html>
<head>
	<style type="text/css">
		.head {
			width: 100%;
			height: 50px;
			float: left;
		}
	</style>
</head>
<body>
	<div class="head">
		<img src="{{url('/images/fornecedor-sakata-logo.png')}}" alt="Image"/>
	</div>
	<div class="container">
		@yield('content')
	</div>
	
</body>
</html>