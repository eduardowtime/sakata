@extends('emails.base')
@section('content')
	<b>SOLICITAÇÃO</b>
	<br>
	<b> Sua solicitação de experimento do trial {{ $solicitation->trial }}/{{ $solicitation->year }} foi criada</b> <br>
	<br>

	<b>DADOS DA SOLICITAÇÃO:</b> <br>
	<b>Número da solicitação: </b>{{ $solicitation->id }}<br>
	<b>Número do Trial: </b>{{ $solicitation->trial }}/{{ $solicitation->year }}<br>
	<b>Cultura: </b>{{ $solicitation->entry->subprogram }} <br>
	<b>Patógeno/Marcador: </b>{{ $solicitation->tools[0]->tool->name }} <br>
	<br>

	<a href="http://biotechapp.sakata.com.br/solicitations/{{ $solicitation->id }}/view">Clique aqui para visualizar a solicitação</a>

	<br><br>
	<small> Notificação automática - Não responder </small>

@endsection