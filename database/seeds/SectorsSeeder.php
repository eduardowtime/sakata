<?php

use Illuminate\Database\Seeder;

class SectorsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('sectors')->insert([
            'name' => 'Melhoria'
        ]);
        DB::table('sectors')->insert([
            'name' => 'Laboratório'
        ]);
    }
}
