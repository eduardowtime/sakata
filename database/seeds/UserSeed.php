<?php

use Illuminate\Database\Seeder;

class UserSeed extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
         DB::table('users')->insert([
            'name' => 'Eduardo Bonete',
            'email' => 'edubonete1@gmail.com',
            'password' => Hash::make('123456'),
            'level' => 3
        ]);
    }
}
