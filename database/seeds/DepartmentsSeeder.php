<?php

use Illuminate\Database\Seeder;

class DepartmentsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('departments')->insert([
            'name' => 'Fito'
        ]);
        DB::table('departments')->insert([
            'name' => 'Bio'
        ]);
    }
}
