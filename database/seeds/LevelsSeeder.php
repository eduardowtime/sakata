<?php

use Illuminate\Database\Seeder;

class LevelsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('levels')->insert([
            'name' => 'Usuário'
        ]);
        DB::table('levels')->insert([
            'name' => 'Admin'
        ]);
        DB::table('levels')->insert([
            'name' => 'Master'
        ]);
    }
}
