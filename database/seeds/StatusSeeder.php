<?php

use Illuminate\Database\Seeder;

class StatusSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('status')->insert([
            'cod' => 0,
            'name' => 'Novo'
        ]);
        DB::table('status')->insert([
            'cod' => 1,
            'name' => 'Em execução'
        ]);
        DB::table('status')->insert([
            'cod' => 2,
            'name' => 'Em avaliação'
        ]);
        DB::table('status')->insert([
            'cod' => 3,
            'name' => 'Reprovado'
        ]);
        DB::table('status')->insert([
            'cod' => 4,
            'name' => 'Finalizado'
        ]);
    }
}
