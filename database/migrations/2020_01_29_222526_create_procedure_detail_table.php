<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateProcedureDetailTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('procedure_detail', function (Blueprint $table) {
            $table->increments('id')->unsigned();
            $table->integer('procedure_id')->unsigned();
            $table->string('title');
            $table->foreign('procedure_id')->references('id')->on('procedure');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('procedure_detail');
    }
}
