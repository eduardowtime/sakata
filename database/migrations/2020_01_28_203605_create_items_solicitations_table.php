<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateItemsSolicitationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('items_solicitations', function (Blueprint $table) {
            $table->increments('id')->unsigned();
            $table->integer('solicitations_id')->unsigned();
            $table->integer('plants');
            $table->boolean('active')->default(1);
            $table->foreign('solicitations_id')->references('id')->on('solicitations');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('items_solicitations');
    }
}
