<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSolicitationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('solicitations', function (Blueprint $table) {
            $table->increments('id')->unsigned();
            $table->integer('department_id')->unsigned();
            $table->integer('trial');
            $table->integer('year');
            $table->integer('tool_id')->unsigned();
            $table->integer('number');
            $table->integer('user_id')->unsigned();
            $table->string('sector');
            $table->date('seeding_date')->nullable();
            $table->date('collect_date')->nullable();
            $table->date('expected_date')->nullable();
            $table->integer('status')->default(0);
            $table->foreign('user_id')->references('id')->on('users');
            $table->foreign('tool_id')->references('id')->on('tools');
            $table->foreign('department_id')->references('id')->on('departments');
            $table->string('obs', 100)->nullable();
            $table->boolean('active')->default(1);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('solicitations');
    }
}
