<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateProcedureTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('procedure', function (Blueprint $table) {
            $table->increments('id')->unsigned();
            $table->integer('protocol_id')->unsigned();
            $table->integer('subprogram_id');
            $table->string('subprogram', 100);
            $table->string('resistance');
            $table->smallInteger('methodology');
            $table->string('description');
            $table->foreign('protocol_id')->references('id')->on('protocol');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('procedure');
    }
}
