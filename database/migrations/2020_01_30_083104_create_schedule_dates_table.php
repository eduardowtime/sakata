<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateScheduleDatesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('schedule_dates', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->integer('schedule_id')->unsigned();
            $table->integer('solicitation_id')->unsigned();
            $table->date('proposed_date');
            $table->date('accomplished_date')->nullable();
            $table->timestamps();
            $table->foreign('schedule_id')->references('id')->on('procedure_schedule');
            $table->foreign('solicitation_id')->references('id')->on('solicitations');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('schedule_dates');
    }
}
