<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateToolsSolicitationTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tools_solicitation', function (Blueprint $table) {
            $table->increments('id');
            $table->increments('solicitation_id')->unsigned();
            $table->increments('tool_id')->unsigned();
            $table->foreign('solicitation_id')->references('id')->on('solicitations');
            $table->foreign('tool_id')->references('id')->on('tools');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tools_solicitation');
    }
}
