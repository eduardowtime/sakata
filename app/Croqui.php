<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Croqui extends Model
{
    protected $table = 'croqui';

    protected $fillable = [
        'solicitation_id',
        'board',
        'index',
        'af',
        'field'
    ];

    public function tool() {
        return $this->hasOne(Tool::class, 'id', 'tool_id');
    }
}
