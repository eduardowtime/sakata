<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class EntryController extends Controller
{
    public function getSectors($trial, $ano)
    {
        return \DB::table('BIO_VW_ENTRIES')
            ->distinct()
            ->select('SET_NOME as id', 'SET_NOME as description')
            ->where('REL_CODIGO', '=', $trial.'/'.$ano)
            ->orderBy('SET_NOME')
            ->get();
    }

    public function getSubProgram($trial, $ano, $setor)
    {
        return \DB::table('BIO_VW_ENTRIES')
            ->distinct()
            ->select('SUBPROGRAM_ID as id', 'SUBPROGRAM as description')
            ->where('REL_CODIGO', '=', $trial.'/'.$ano)
            ->where('set_nome', '=', $setor)
            ->get();
    }

    public function getProject($trial, $ano, $setor, $subp)
    {
        return \DB::table('BIO_VW_ENTRIES')
            ->distinct()
            ->select('BIO_VW_ENTRIES.PROJETO as id', 'BIO_VW_PROJETOS.DESC_PROJECT as description')
            ->join('BIO_VW_PROJETOS', 'BIO_VW_PROJETOS.ID_PROJECT', '=', 'PROJETO')
            ->where('REL_CODIGO', '=', $trial.'/'.$ano)
            ->where('set_nome', '=', $setor)
            ->where('BIO_VW_ENTRIES.SUBPROGRAM_ID', '=', $subp)
            ->get();
    }

    public function setAf(Request $request)
    {
        $entries =  \DB::table('BIO_VW_ENTRIES')
            ->select('ID_ENTRIES', 'LINE')
            ->where('REL_CODIGO', '=', str_pad($request->trial, 3, '0', STR_PAD_LEFT).'/'.$request->ano)
            ->where('set_nome', '=', $request->sector_id)
            ->where('SUBPROGRAM_ID', '=', $request->subprogram_id)
            ->where('PROJETO', '=', $request->project_id)
            ->get();

        foreach ($entries->all() as $key => $value) {
            \DB::table('ITEMS_SOLICITATIONS')
            ->insert([
                'solicitations_id' => $request->id_sol,
                'item_id' => $value->id_entries,
                'line' => $value->line,
                'plants' => 0
            ]);
        }
    }
}
