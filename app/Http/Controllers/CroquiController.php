<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Repository\CroquiRepository;
use App\Croqui;
use App\Solicitation;
use App\Entry;
use Mail;

class CroquiController extends Controller
{

    public function __construct()
    {
        $this->repository = new CroquiRepository;
    }


    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $numBoards = Croqui::select('board')
            ->where('solicitation_id', $request->solicitation_id)->distinct()->count();
        $result = array(
            'boards' => $this->repository->getNumBoards($request->solicitation_id),
            'fields' => $this->repository->setFields($request->solicitation_id, $request->board),
            'items' => $this->repository->itemsValue($request->solicitation_id),
            'board' => $this->repository->getBoard($request->solicitation_id, $request->board),
            'finalized' => $this->repository->verifyRecord($request->solicitation_id)
        );
        return $result;
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $fields = $this->repository->getFields();
        $index = array_search($request->field, $fields)+1;
        if($index <= 0 ){
            return response(500)->json(['message' => 'Coluna ou linha inválida']);
        }
        $end = $index + $request->qtd;
        while($index < $end) {
            Croqui::create([
                'solicitation_id' => $request->solicitation_id,
                'board' => $request->board,
                'af' => $request->af,
                'index' => $this->repository->setIndex($request->af, $request->solicitation_id),
                'field' => $fields[$index-1]
            ]);
            $index++;
        }

        $data = Solicitation::with('user')
            ->with('department')
            ->with('tools')
            ->with('items')
            ->with('status')
            ->where('id', $request->solicitation_id)
            ->first();

        $data->entry = $this->getEntry($data);

        Mail::send('emails.status', array("solicitation" => $data, 'status' => "Croqui do experimento foi criado com sucesso" ), function($message) use ($data) {
            $message->subject('Status de solicitação: '.$data->id)
                ->from('sistema.biofito@sakata.com.br');

            $message->to($data->user->email);
        });

    }

    public function getEntry($data) 
    {
        $entry = Entry::where('subprogram_id', $data->subprogram_id)->first();
        return $entry;
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    public function range(Request $request)
    {
        return Croqui::select('index')->where('solicitation_id', $request->solicitation_id)
            ->where('af', $request->af)
            ->max('index');

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function reset(Request $request)
    {
        return Croqui::where('solicitation_id', $request->solicitation_id)
            ->where('board', $request->board)
            ->delete();
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
