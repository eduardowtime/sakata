<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Schedule;

class ScheduleController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        if ($request->accomplished_date == ''){
            Schedule::updateOrInsert(
                ['schedule_id' => $request->id, 'solicitation_id' => $request->solicitation_id],
                ['proposed_date' => \Carbon\Carbon::parse($request->proposed_date)->format('Y-m-d')]
            );
        } else {
            Schedule::updateOrInsert(
                ['schedule_id' => $request->id, 'solicitation_id' => $request->solicitation_id],
                ['proposed_date' => \Carbon\Carbon::parse($request->proposed_date)->format('Y-m-d'), 
                'accomplished_date' => \Carbon\Carbon::parse($request->accomplished_date)->format('Y-m-d')]
            );
        }
        return response('ok', 200);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
