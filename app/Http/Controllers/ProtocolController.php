<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Repository\ProtocolRepository;
use App\Protocol;

class ProtocolController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $protocol = Protocol::select('*');
        if($request->has('active') && $request->active >= 0) {
            $protocol->where('active', $request->active);
        }
        if($request->has('name') && $request->name != '') {
            $nome = strtolower($request->name);
            $protocol->whereRaw('lower(name) LIKE (?)', "%{$nome}%");
        }
        return $protocol->orderBy('name')->get();
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $protocol = new Protocol;
        $protocol->name = $request->name;
        $protocol->save();
        return $protocol;
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        return Protocol::where('id', $id)
            ->with('procedure')
            ->first();
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $protocol = Protocol::find($id);
        $protocol->fill($request->all());
        $protocol->save();
        return $protocol;
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    /**
     * Clone the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function clone($id, ProtocolRepository $repository)
    {
        return $repository->clone($id);
    }
}
