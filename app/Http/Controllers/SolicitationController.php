<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Solicitation;
use App\Entry;
use App\Report;
use App\User;
use App\Repository\HistoryRepository;
use App\Repository\SolicitationRepository;
use App\Repository\ToolRepository;
use Mail;

/**
 * Solicitações
 */
class SolicitationController extends Controller
{

    public function __construct()
    {
        $this->repository = new SolicitationRepository;
    }

    /**
     * Display a listing of the resource.
     * 
     * @param \Illuminate\Http\Request $request 
     * 
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $solicitation = Solicitation::with('user')
            ->with('department')
            ->with('tools')
            ->with('items')
            ->with('status');
        $resource = new \App\Http\Resources\UserResource;
        $userDepartment = $resource->userDepartment($request->user()->id);
        
        if (count($userDepartment)>0) {
            $solicitation->where('department_id', $userDepartment[0]->id);
        } else if (!$resource->isMaster($request->user()->id)) {
            $solicitation->where('user_id', $request->user()->id);
        }
        $solicitation->where('active', 1);



        return $solicitation->orderBy('id', 'desc')->paginate(10);
    }

    public function find(Request $request)
    {
        $solicitation = Solicitation::with('user')
            ->with('department')
            ->with('tools')
            ->with('items')
            ->with('status');
        $resource = new \App\Http\Resources\UserResource;
        $userDepartment = $resource->userDepartment($request->user()->id);
        
        $solicitation->where('active', 1);

        if (count($userDepartment)>0) {
            $solicitation->where('department_id', $userDepartment[0]->id);
        } else if (!$resource->isMaster($request->user()->id)) {
            $solicitation->where('user_id', $request->user()->id);
        }
        if($request->status != null && $request->status != '') {
            $solicitation->where('status', $request->status);
        }
        if($request->has('start_date') && !is_null($request->start_date)) {
            $start_date = \Carbon\Carbon::createFromFormat('d/m/Y',$request->start_date)->format('Y-m-d 00:00:01');
            $solicitation->where(
                'created_at', '>=', 
                $start_date
            );
        }

        if($request->has('end_date') && !is_null($request->end_date)) {
            $end_date = \Carbon\Carbon::createFromFormat('d/m/Y',$request->end_date)->format('Y-m-d 23:59:59');
            $solicitation->where(
                'created_at', '<=', 
                $end_date
            );
        }
        if($request->mix != '') {
            $solicitation->whereIn(
                'user_id', function ($query) use ($request) {
                    $query->select('id')
                        ->from('users')
                        ->whereRaw(
                            'LOWER(name) like ?', '%'.strtolower($request->mix).'%'
                        )->get();
                }
            );
        }
        return $solicitation->paginate(15);
    }


    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function record(Request $request)
    {
        return Solicitation::where('id', $request->solicitation_id)->update([
            'croqui' => 1
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request 
     * 
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, HistoryRepository $repository)
    {
        $tools = new ToolRepository;
        $solicitation = new Solicitation;
        $solicitation->department_id = $request->department_id;
        $solicitation->user_id = $request->user()->id;
        $solicitation->trial = $request->trial;
        $solicitation->year = $request->year;
        $solicitation->sector_id = $request->sector_id;
        $solicitation->subprogram_id = $request->subprogram_id;
        $solicitation->project_id = $request->project_id;
        $solicitation->number = $request->number;
        $solicitation->obs = $request->obs;
        $solicitation->save();

        $repository->add(
            $request->user()->id,
            $solicitation->id,
            0,
            'Solicitação criada'
        );
        $tools->addTools($solicitation->id, $request->tools);
        $data = Solicitation::with('user')
            ->with('department')
            ->with('tools')
            ->with('items')
            ->with('statusb')
            ->where('id', $solicitation->id)
            ->first();

        $data->entry = $this->getEntry($solicitation);
        $mails = $this->getMails($solicitation->department_id);


        Mail::send('emails.created', array("solicitation" => $data ), function($message) use ($mails) {
            $message->subject('Nova solicitação')
                ->from('sistema.biofito@sakata.com.br');

            $message->to($mails);
        });

        Mail::send('emails.response', array("solicitation" => $data ), function($message) use ($data) {
            $message->subject('Solicitação criada')
                ->from('sistema.biofito@sakata.com.br');

            $message->to($data->user->email);
        });

        return $solicitation;
    }

    public function getMails($department) 
    {
        $mails = User::select('email')
            ->join('laboratories', 'laboratories.id', 'users.laboratory_id')
            ->where('laboratories.department_id', $department)
            ->get();
        $email = array();
        foreach ($mails->all() as $key => $value) {
            $email[] = $value['email'];
        }
        return $email;
    }

    public function getEntry($data) 
    {
        $entry = Entry::where('subprogram_id', $data->subprogram_id)->first();
        return $entry;
    }

    /**
     * Display the specified resource.
     *
     * @param int $id Id da solicitação
     * 
     * @return \Illuminate\Http\Response
     */
    public function show($id, SolicitationRepository $repository)
    {
        $sol =  Solicitation::with('user')
            ->with('department')
            ->with('tools')
            ->with('status')
            ->with('items')
            ->with('procedure')
            ->where('active', 1)
            ->where('id', $id)
            ->first();

        $sol->protocol_ok = $repository->protocolOk($id);
        $sol = json_decode($sol, true);
        $sol['procedure']['activities'] 
            = \App\Schedule::select(
                'procedure_schedule.id', 
                'initial_day', 
                'end_day', 
                'activities', 
                'proposed_date', 
                'accomplished_date'
            )
            ->rightJoin(
                'procedure_schedule', function ($join) use ($sol) {
                    $join->on('schedule_dates.schedule_id', 'procedure_schedule.id');
                    $join->on('schedule_dates.solicitation_id', '=',  $sol['id']);
                }
            )
            ->where('procedure_schedule.procedure_id', '=', $sol['protocol_id'])
            ->orderBy('procedure_schedule.id')
            ->get();
        $sol['procedure']['descriptive']
            = \App\Descriptive::where('procedure_id', '=', $sol['protocol_id'])->first();


        return $sol;
    }


    /**
     * Display the resource with complete protocol data.
     *
     * @param int $id Id do protocolo
     * 
     * @return \Illuminate\Http\Response
     */
    public function allProtocol($id)
    {
        $result = Solicitation::where('id', $id)
            ->where('active', 1)
            ->with('procedure')
            ->first();
        $result = json_decode($result, true);

        $result['procedure']['schedule'] 
            = \App\Schedule::select(
                'procedure_schedule.id', 
                'initial_day', 
                'end_day', 
                'activities', 
                'proposed_date', 
                'accomplished_date'
            )
            ->rightJoin(
                'procedure_schedule', function ($join) use ($result) {
                    $join->on('schedule_dates.schedule_id', 'procedure_schedule.id');
                    $join->on('schedule_dates.solicitation_id', '=',  $result['id']);
                }
            )
            ->where('procedure_schedule.procedure_id', '=', $result['protocol_id'])
            ->orderBy('procedure_schedule.id')
            ->get();

        $result['procedure']['descriptions'] 
            = \App\Description::where('procedure_id', '=', $result['protocol_id'])
            ->with('texts')
            ->orderBy('procedure_detail.id')
            ->get();

        return $result;

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id Id da solicitação
     * 
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the status id from solicitation
     *
     * @param \Illuminate\Http\Request $request 
     * @param int                      $id 
     * 
     * @return \Illuminate\Http\Response
     */
    public function setStatus(
        Request $request, $id, 
        HistoryRepository $history, 
        SolicitationRepository $repository
    )
    {
        $sol = $repository->updateStatus($id, $request->status);
        $history->add(
            $request->user()->id,
            $id,
            $request->status,
            'Alteração de status da solicitação'
        );

        $data = Solicitation::with('user')
            ->with('department')
            ->with('tools')
            ->with('items')
            ->with('status')
            ->where('id', $id)
            ->where('active', 1)
            ->first();

        $data->entry = $this->getEntry($data);

        Mail::send('emails.status', array("solicitation" => $data, 'status' => "Seu experimento foi alterado para o status: " ), function($message) use ($data) {
            $message->subject('Status de solicitação: '.$data->id)
                ->from('sistema.biofito@sakata.com.br');

            $message->to($data->user->email);
        });
        return $sol;
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request 
     * @param int                      $id 
     * 
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id, HistoryRepository $repository)
    {
        if($request->has('protocol_id')) {
            $data = Solicitation::with('user')
                ->with('department')
                ->with('tools')
                ->with('items')
                ->with('status')
                ->where('id', $id)
                ->where('active', 1)
                ->first();

            $data->entry = $this->getEntry($data);

            Mail::send('emails.status', array("solicitation" => $data, 'status' => "Programação do experimento foi adicionado com sucesso" ), function($message) use ($data) {
                $message->subject('Status de solicitação: '.$data->id)
                    ->from('sistema.biofito@sakata.com.br');

                $message->to($data->user->email);
            });
        }
        $solicitation = Solicitation::find($id);
        $solicitation->fill($request->all());
        $solicitation->push();
        $repository->add(
            $request->user()->id,
            $id,
            0,
            'Solicitação alterada'
        );
        return $solicitation;
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id 
     * 
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $solicitation = Solicitation::find($id);
        $solicitation->active = 0;
        $solicitation->save();
        return response()->json(['message'=> 'Solicitação excluida com sucesso'], 200);
    }

    public function saveReport(Request $request)
    {
        $report = Report::updateOrCreate(
            ['solicitation_id' => $request->id],
            ['report' => $request->content]
        );
        return $report;
    }

    public function report($solicitation_id)
    {
        $report = Report::where('solicitation_id', $solicitation_id)->first();
        if(is_null($report)) {
            $report = array("report"=> $this->repository->textReport($solicitation_id));
        }
        return $report;
    }

}