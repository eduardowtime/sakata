<?php

namespace App\Http\Controllers;
use Illuminate\Support\Facades\Storage;
use App\Attachment;

use Illuminate\Http\Request;

class AttachmentController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        if($request->has('solicitation_id')) {
            return Attachment::with('user')
                ->where('solicitation_id', $request->solicitation_id)->get();
        }

        return Attachment::with('user')->get();
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $file = $request->file('file');
        $fileName = \Carbon\Carbon::now().'.'.$file->extension();
        $file->storeAs('public', $fileName);
        $att = new Attachment;
        $att->solicitation_id = $request->solicitation_id;
        $att->description = $request->description;
        $att->file = $fileName;
        $att->user_id = $request->user()->id;
        $att->save();
        return $att;
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    /**
     * Save file to storage.
     *
     * @param \Illuminate\Http\Request $request 
     * 
     * @return string $file
     */
    public function upload(Request $request) 
    {
        $file = $request->file('file');
        $fileName = $file->getClientOriginalName().'.'.$file->extension();
        $file->storeAs('public', $fileName);
        return $file;
    }

    public function download(Request $request)
    {
        return response()->download(storage_path("app/public/{$request->file}"));
    }
}
