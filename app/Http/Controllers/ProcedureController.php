<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Procedure;

class ProcedureController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return Procedure::get();
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $procedure = new Procedure;
        $procedure->protocol_id = $request->protocol_id;
        $procedure->subprogram_id = $request->subprogram['id'];
        $procedure->subprogram = $request->subprogram['name'];
        $procedure->resistance = $request->resistance;
        $procedure->methodology = $request->methodology;
        $procedure->description = $request->description;
        $procedure->save();
        return $procedure;
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        return Procedure::where('id', $id)
            ->with('activities')
            ->with('descriptive')
            ->first();
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function showByProtocol($id)
    {
        return Procedure::where('protocol_id', $id)
            ->with('activities')
            ->with('descriptive')
            ->get();
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $procedure = Procedure::find($id);
        $procedure->protocol_id = $request->protocol_id;
        $procedure->subprogram_id = $request->subprogram['id'];
        $procedure->subprogram = $request->subprogram['name'];
        $procedure->resistance = $request->resistance;
        $procedure->methodology = $request->methodology;
        $procedure->description = $request->description;
        $procedure->save();
        return $procedure;
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        return Procedure::destroy($id);
    }
}
