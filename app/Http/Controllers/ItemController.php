<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Item;
use App\Repository\HistoryRepository;
use App\Repository\SolicitationRepository;

class ItemController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        return $request;
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        return Item::create([
            'solicitations_id' => $request->solicitations_id,
            'plants' => $request->plants,
            'item_id' => $request->af,
            'rs' => $request->rs,
            'active' => 1,
            'witness' => 1
        ]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }


    public function record(Request $request)
    {
        foreach ($request->all() as $key => $value) {
            Item::where('id', $value['id'])
                ->update(['plants' => $value['plants'], 'rs' => $value['rs']]
            );
        }
        return response('OK', 200);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }


    /**
     * Get a list of items by id of solicitation
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function getBySolicitation(
        Request $request, $id, 
        HistoryRepository $repository,
        SolicitationRepository $sRepository
    )
    {

        $items = Item::where('solicitations_id', $id)->orderBy('item_id')->get();
        if(count($items) == 0) {
            $sol = \App\Solicitation::find($id);
            $entries =  \DB::table('BIO_VW_ENTRIES')
                ->select('ID_ENTRIES', 'LINE')
                ->where('REL_CODIGO', '=', str_pad($sol['trial'], 3, '0', STR_PAD_LEFT).'/'.$sol['year'])
                ->where('set_nome', '=', $sol['sector_id'])
                ->where('SUBPROGRAM_ID', '=', $sol['subprogram_id'])
                ->where('PROJETO', '=', $sol['project_id'])
                ->get();


            \DB::table('items_solicitations')->where('solicitations_id', $id)->delete();
            foreach ($entries->all() as $key => $value) {
                \DB::table('ITEMS_SOLICITATIONS')
                ->insert([
                    'solicitations_id' => $id,
                    'item_id' => $value->id_entries,
                    'line' => $value->line,
                    'plants' => 0
                ]);
            }
            $items = Item::where('solicitations_id', $id)->orderBy('item_id')->get();
            $repository->add(
                $request->user()->id,
                $id,
                1,
                'Itens adquiridos do SISDA'
            );
            $sRepository->updateStatus($id, 1);
            $sRepository->updateUser($id, $request->user()->id);
        }            

        return $items;
    }
}
