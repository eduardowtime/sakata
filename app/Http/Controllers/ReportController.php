<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Repository\SolicitationRepository;

class ReportController extends Controller
{
    public function solicitations(Request $request, SolicitationRepository $repository)
    {
        return $repository->report($request);
    }
}
