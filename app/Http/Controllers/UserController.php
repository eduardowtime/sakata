<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use App\Repository\LogRepository;
use App\Http\Resources\UserResource;
use App\User;

class UserController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return 
        User::with('levels')
            ->with('laboratory')
            ->with('sector')
            ->where('deleted_at', null)
            ->get();
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $action = 'update';
        if($request->id == null){
            $request['password'] = Hash::make('12345678');
            $action = 'insert';
        }
        $user = User::updateOrCreate(
            ['id'=> $request->id],
            $request->all()
        );
        LogRepository::save($request->user()->id, $action, 'users', $user->id);
        return $user;
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        return 
        User::with('levels')
            ->with('laboratory')
            ->with('sector')
            ->where('id', $id)
            ->first();
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    public function updatePassword(Request $request, $id)
    {
        $user = User::find($id);
        $user->password = Hash::make($request->password);
        $user->save();
        return response()->json(['message'=> 'Senha alterada com sucesso']);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, $id)
    {
        LogRepository::save($request->user()->id, 'delete', 'users', $id);
        return User::where('id', $id)
            ->update(['deleted_at' => \Carbon\Carbon::now()]);
    }

    /**
     * Return data from logged user
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function loggedUser(Request $request, UserResource $resource)
    {
        $user = 
            User::with('levels')
            ->with('laboratory')
            ->with('sector')
            ->where('id', $request->user()->id)
            ->firstOrFail();
        $profile = $resource->getProfile($user->id);
        $user['profile'] = $profile;
        $user['permissions'] = $resource->permissions($profile);
        return $user;
    }
}
