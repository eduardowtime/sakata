<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\View;
use Mail;

class ViewController extends Controller
{
    public function showProjects() {
        return View::distinct('id_project')
            ->select('id_project','desc_project')
            ->orderBy('desc_project')
            ->get();
    }

    public function findProjects($id) {
        return View::distinct('id_project')
            ->select('id_project','desc_project')
            ->where('id_project', '=', $id)
            ->get();
    }

    public function showSubPrograms() {
        return View::distinct('id_sub_program')
            ->select('id_sub_program as id','desc_subprogram as name')
            ->orderBy('id_sub_program')
            ->get();
    }

    public function findSubPrograms($id) {
        return View::distinct('id_sub_program')
            ->select('id_sub_program','desc_subprogram')
            ->where('id_sub_program', '=', $id)
            ->get();
    }

    public function showSectors() {
        return \DB::table('BIO_VW_BREEDING')
            ->distinct()
            ->select(\DB::raw('CONCAT(CONCAT(set_dpt_codigo, \'-\'), set_codigo) as id'), 'BREEDING_SECTION as description')
            //->select('CONCAT(set_dpt_codigo, "-") ', 'set_codigo', 'BREEDING_SECTION')
            ->orderBy('BREEDING_SECTION')
            ->get();
    }

    public function mail() {
        Mail::send('emails.created', array("teste" => "teste"), function($message) {
            $message->to('edubonete1@gmail.com', 'Eduardo Bonete')
                ->subject('Email de teste')
                ->from('system@sakata.com.br');
        });
        return 'Email enviado com sucesso';
    }
}
