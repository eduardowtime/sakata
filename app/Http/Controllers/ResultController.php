<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Repository\ResultRepository;
use App\Result;

class ResultController extends Controller
{
    public function __construct()
    {
        $this->repository = new ResultRepository;
    }

    public function index(Request $request)
    {
        return $this->repository->result($request->all());
    }

    public function save($id, Request $request)
    {
        foreach ($request->all() as $key => $value) {
            foreach ($value['tools'] as $k => $v) {
                if(!is_null($v['result'])) {
                    Result::updateOrCreate(
                        ['croqui_id' => $value['id'], 'tool_id'=> $v['id']],
                        [
                            'croqui_id'=> $value['id'],
                            'tool_id' => $v['id'],
                            'result' => $v['result']
                        ]
                    );
                }
            }
        }
    }
}