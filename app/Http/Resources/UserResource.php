<?php

namespace App\Http\Resources;

use App\User;

/**
 * 
 */
class UserResource
{
    
    public function userDepartment($id)
    {
        return User::select('departments.id')
            ->join('laboratories', 'laboratories.id', 'users.LABORATORY_ID')
            ->join('departments', 'departments.id', 'laboratories.department_id')
            ->where('users.id', $id)
            ->get();
    }

    public function isMaster($id)
    {
        $user = User::with('levels')
            ->where('id', $id)
            ->first();
        return $user['levels']['name'] == 'Master';
    }

    public function getProfile($id)
    {
        $user = User::select('*')
            ->with('levels')
            ->with('laboratory')
            ->where('users.id',$id)->first();
        if($user->levels->name == 'Master') {
            return 'master';
        } else if($user->levels->name == 'Admin'){
            if($user->laboratory->department->name == 'SFT') {
                return 'adm.sft';
            } else {
                return 'adm.sbi';
            }
        } else {
            if($user->laboratory_id ==  null) {
                return 'user';
            } else {
                if($user->laboratory->department->name == 'SFT') {
                    return 'user.sft';
                } else {
                    return 'user.sbi';
                }
            }
        }
    }

    public function permissions($profile)
    {
        $permissions =  \DB::table('profiles')
            ->select('permissions.module as permission')
            ->join('profiles_permissions', 'profiles_permissions.profile_id', 'profiles.id')
            ->join('permissions', 'permissions.id', 'profiles_permissions.permission_id')
            ->where('profiles.profile', $profile)
            ->get();
        $result = array();
        foreach ($permissions->all() as $key => $value) {
            $result[] = $value->permission;
        }
        return $result;
    }
}