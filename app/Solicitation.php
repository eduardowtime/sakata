<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Solicitation extends Model
{

    protected $fillable = [
        'id', 
        'department_id',
        'trial',
        'year',
        'tool_id',
        'number',
        'obs',
        'protocol_id',
        'itens_obs',
    ];

    public function user() {
        return $this->hasOne(User::class, 'id', 'user_id');
    }

    public function tools() {
        return $this->hasMany(ToolSolicitation::class, 'solicitation_id', 'id')->with('tool');
    }

    public function department() {
        return $this->hasOne(Department::class, 'id', 'department_id');
    }

    public function status() {
        return $this->hasOne(Status::class, 'cod', 'status');
    }

    public function statusb() {
        return $this->hasOne(Status::class, 'cod', 'status');
    }

    public function getCreatedAtAttribute($value)
    {
        return \Carbon\Carbon::parse($value)->format('d/m/Y H:i');
    }

    public function items() {
        return $this->hasMany(Item::class, 'solicitations_id', 'id')->orderBy('item_id');
        
    }

    public function procedure() {
        return $this->hasOne(Procedure::class, 'id', 'protocol_id')
        ->with('activities')
        ->with('descriptive');
    }

}
