<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Activity extends Model
{
    protected $table = 'procedure_schedule';
    public $timestamps = false;
}
