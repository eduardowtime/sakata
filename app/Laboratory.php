<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Laboratory extends Model
{
    protected $table = 'laboratories';

    public function department() {
        return $this->hasOne(Department::class, 'id', 'department_id');
    }
}
