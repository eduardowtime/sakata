<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Procedure extends Model
{
    protected $table = 'procedure';
    public $timestamps = false;

    protected $fillable = [
        'protocol_id',
        'subprogram_id',
        'subprogram'
    ];


    public function activities() {
        return $this->hasMany(\App\Activity::class, 'procedure_id', 'id');
    }

    public function descriptive() {
        return $this->hasOne(\App\Descriptive::class, 'procedure_id', 'id');
    }

}
