<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Item extends Model
{
    protected $table = 'items_solicitations';

    protected $fillable = [
        'solicitations_id',
        'plants',
        'item_id',
        'active',
        'witness'
    ];
    
}
