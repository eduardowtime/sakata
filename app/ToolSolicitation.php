<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ToolSolicitation extends Model
{
    protected $table = 'tools_solicitation';

    public function tool()
    {
        return $this->hasOne(Tool::class, 'id', 'tool_id');
    }
}
