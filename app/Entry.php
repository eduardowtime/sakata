<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Entry extends Model
{
    protected $table = 'BIO_VW_ENTRIES';
    public $timestamps = false;
}