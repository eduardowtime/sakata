<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Descriptive extends Model
{
    protected $table = 'descriptive';

    protected $fillable = [
        'descriptive',
        'procedure_id'
    ];
}
