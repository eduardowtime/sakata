<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Protocol extends Model
{
    protected $table = 'protocol';

    protected $fillable = [
        'name', 'active'
    ];

    public function procedure() {
        return $this->hasMany(\App\Procedure::class, 'protocol_id', 'id')
        ->with('activities')
        ->with('descriptive');
    }

    public function getCreatedAtAttribute($date)
    {
        return \Carbon\Carbon::parse($date)->format('d/m/Y');
    }
}
