<?php
namespace App;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Laravel\Passport\HasApiTokens;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;

class User extends Authenticatable
{
    use HasApiTokens, Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'sector_id', 'laboratory_id', 'password', 'level'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    public function levels() {
        return $this->hasOne(Level::class, 'id', 'level');
    }

    public function sector() {
        return $this->hasOne(Sector::class, 'id', 'sector_id');
    }

    public function laboratory() {
        return $this->hasOne(Laboratory::class, 'id', 'laboratory_id')->with('department');
    }
}
