<?php

namespace App\Repository;

use App\History;

class HistoryRepository
{
    /**
     * Atualização de status da solicitação
     * 
     * @param int $solicitationId 
     * @param int $statusId 
     * 
     * @return static null 
     */
    public function add($userId, $solicitationId, $statusId, $obs) 
    {
        $history = new History;
        $history->user_id = $userId;
        $history->solicitation_id = $solicitationId;
        $history->status_id = $statusId;
        $history->obs = $obs;
        $history->save();
        return $history;

    }
}