<?php

namespace App\Repository;

use App\Protocol;
use App\Repository\ProcedureRepository;

class ProtocolRepository
{
    /**
     * Atualização de status da solicitação
     * 
     * @param int $solicitationId 
     * @param int $statusId 
     * 
     * @return static null 
     */

    public function clone($id)
    {
        $protocol = Protocol::find($id);
        $protocol->active = 0;
        $protocol->old = 1;
        $protocol->save();
        $new = new Protocol;
        $new->name = $protocol->name;
        $new->save();

        $procedure = new ProcedureRepository;
        $procedure = $procedure->clone($id, $new->id);
        
        return $new;
    }
}