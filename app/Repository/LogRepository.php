<?php

namespace App\Repository;

use App\Log;

class LogRepository
{
    public static function save($user, $action, $table, $item_id)
    {
        Log::create([
            'user_id' => $user,
            'action' => $action,
            'table' => $table,
            'item_id' => $item_id
        ]);
    }
}