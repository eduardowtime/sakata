<?php

namespace App\Repository;

use App\Solicitation;
use App\User;
use App\Repository\ScheduleRepository;
use App\Repository\ProcedureRepository;

class SolicitationRepository
{
    /**
     * Atualização de status da solicitação
     * 
     * @param int $solicitationId 
     * @param int $statusId 
     * 
     * @return static null 
     */
    public function updateStatus($solicitationId, $statusId) 
    {
        return Solicitation::where('id', $solicitationId)
        ->update(['status' => $statusId]);

    }


    public function updateUser($solicitationId, $userId) 
    {
        return Solicitation::where('id', $solicitationId)
        ->update(['user_accept' => $userId]);

    }
    
    /**
     * Verifica se as datas de execução estão preenchidas
     * para o protocolo
     * 
     * @param int $solicitationId 
     * 
     * @return boolean  
     */
    public function protocolOk($solicitationId)
    {
        $schedule = new ScheduleRepository;
        $procedure = new ProcedureRepository;
        $solicitation = Solicitation::where('id', $solicitationId)->first();
        $scheduleCount =  $schedule->count($solicitationId);
        $procedureSchedules 
            =  $procedure->countSchedules($solicitation->protocol_id);
        
        return $scheduleCount == $procedureSchedules;

    }

    public function report($request)
    {
        $solicitation = Solicitation::with('user')
            ->with('department')
            ->with('tools');
        $solicitation->select(
            'solicitations.id',
            'solicitations.trial',
            'solicitations.year',
            'solicitations.user_id',
            'solicitations.subprogram_id',
            'solicitations.project_id',
            'solicitations.protocol_id',
            'solicitations.sector_id',
            'solicitations.created_at',
            'solicitations.number'
        )->where('status', 4);

        if($request->has('start_date') && !is_null($request->start_date)) {
            $start_date = \Carbon\Carbon::createFromFormat('d/m/Y',$request->start_date)->format('Y-m-d 00:00:01');
            $solicitation->where(
                'created_at', '>=', 
                $start_date
            );
        }

        if($request->has('end_date') && !is_null($request->end_date)) {
            $end_date = \Carbon\Carbon::createFromFormat('d/m/Y',$request->end_date)->format('Y-m-d 23:59:59');
            $solicitation->where(
                'created_at', '<=', 
                $end_date
            );
        }
        if($request->has('trial') && !is_null($request->trial)) {
            $solicitation->where('trial', $request->trial);
        }
        if($request->has('year') && !is_null($request->year)) {
            $solicitation->where('year', $request->year);
        }
        if($request->has('tools') && count($request->tools) > 0) {
            $tools = array();
            foreach ($request->tools as $key => $value) {
                $tools[] = $value['id'];
            }
            $solicitation->whereIn('tool_id', $tools);
        }
        if($request->has('protocol_id') && !is_null($request->protocol_id)) {
            $solicitation->whereIn('protocol_id', $this->listProcedures($request->protocol_id));
        }
        if($request->has('project_id') && !is_null($request->project_id)) {
            $solicitation->where('project_id', $request->project_id);
        }
        if($request->has('method_id') && !is_null($request->method_id)) {
            $solicitation->where('protocol_id', $this->getProcedure($request->protocol_id, $request->method_id));
        }
        if($request->has('af') && !is_null($request->af)) {
            $solicitation->join('items_solicitations', 'solicitations_id', 'solicitations.id');
            $solicitation->where('items_solicitations.item_id', $request->af);
            $solicitation->distinct();
        }

        return $solicitation->get();
    }

    public function listProcedures($protocol_id)
    {
        $repository = new ProcedureRepository;
        return $repository->listProcedureId($protocol_id);
    }

    public function getProcedure($protocol_id, $method)
    {
        $repository = new ProcedureRepository;
        return $repository->getProcedureId($protocol_id, $method);
    }

    public function getQtd($solicitation) 
    {
        $res = Solicitation::where('id', $solicitation)->first();
        return $res->number;
    }

    public function getTrial($id)
    {
        $solicitation = Solicitation::where('id', $id)->first();
        $trial = "$solicitation->trial/$solicitation->year";
        return $trial;
    }

    public function getSector($id)
    {
        $solicitation = Solicitation::where('id', $id)->first();
        return $solicitation->sector_id;
    }

    public function getSubPrograma($id)
    {
        $solicitation = Solicitation::where('id', $id)->first();
        return $solicitation->subprogram_id;
    }

    public function getPPS($id)
    {
        $solicitation = Solicitation::where('id', $id)->first();
        $ano = \Carbon\Carbon::createFromFormat('d/m/Y H:i', $solicitation->created_at);
        $ano = $ano->year;
        $pps = "PPS($id/$ano)";
        return $pps;
    }

    public function getTotalPlants($id)
    {
        $solicitation = Solicitation::where('id', $id)->first();
        return $solicitation->number;
    }

    public function getProject($trial, $subp)
    {
        $data = \DB::table('BIO_VW_PROJETOS')
            ->where('TrialN', $trial)
            ->where('ID_SUB_PROGRAM', $subp)
            ->first();
        return $data;
    }

    public function getUserAccept($id)
    {
        $solicitation = Solicitation::select('users.name')
            ->join('users', 'users.id', 'user_accept')
            ->where('solicitations.id', $id)
            ->first();
        return $solicitation->name;
    }

    public function textReport($id)
    {
        $pps = $this->getPPS($id);
        $trial = $this->getTrial($id);
        $subp = $this->getSubPrograma($id);
        $tr = $this->getProject($trial, $subp);
        $subprogram = $tr->subprogram;
        $project = $tr->desc_project;
        $trialFrom = \Carbon\Carbon::createFromFormat('Y-m-d H:i:s', $tr->trialperiod)->format('F/Y');
        $trialTo = \Carbon\Carbon::createFromFormat('Y-m-d H:i:s', $tr->to)->format('F/Y');
        $trial_period = "$trialFrom to $trialTo";
        $trial_objective = $tr->trialobjective;
        $total_plants = $this->getTotalPlants($id);
        $user = $this->getUserAccept($id);
        $hoje = \Carbon\Carbon::now()->format('d/m/Y');
        $text = 
            "<br>
            <p><strong>SUBPROGRAM:</strong> $subprogram  </p><br>
            <p><strong>PROJECT: </strong>  $project </p><br>
            <p><strong>TECHNICAL SUPPORT SECTION: </strong>   </p><br>
            <p><strong>TRIAL Nº: </strong>  $trial $pps </p><br>
            <p><strong>LOCATION: </strong>   </p><br>
            <p><strong>TRIAL PERIOD: </strong> $trial_period </p><br>
            <p><strong>TRIAL OBJECTIVE: </strong> $trial_objective</p><br>
            <p><strong>MATERIALS AND METHODS: </strong>  </p>
            <p>   Isolate:   </p>
            <p>  Number of Inoculations: </p>
            <p> Inoculation Method:   </p>
            <p>   Inoculum Concentration: </p>
            <p> Evaluation: </p>
            <p> Total Number of Plants: $total_plants</p>
            <p> Number of Plants per Plastic Bags / Pots:    </p><br>
            <p><strong>RESULTS AND CONCLUSIONS: </strong>  </p>
            <p><br></p>
            <p><strong>RESPONSIBLE:</strong> $user</p><br><br>
            <p><strong>DATE: </strong> $hoje </p>

            <p class='ql-align-right'>____________________</p>
            <p class='ql-align-right'><strong>Plant Pathologist          </strong></p>
            <p class='ql-align-right'> $user</p>";
        return $text;
    }
}
