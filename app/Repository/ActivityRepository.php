<?php

namespace App\Repository;

use App\Activity;

class ActivityRepository
{
    /**
     * Atualização de status da solicitação
     * 
     * @param int $solicitationId 
     * @param int $statusId 
     * 
     * @return static null 
     */
    public function countSchedules($id) 
    {
        return \DB::table('PROCEDURE_SCHEDULE')->where('procedure_id', $id)->count();
    }

    public function clone($procedure, $newProcedure)
    {
        $activity = Activity::where('procedure_id', $procedure)->get();
        foreach ($activity->all() as $key => $value) {
            $new = new Activity;
            $new->procedure_id = $newProcedure;
            $new->initial_day = $value['initial_day'];
            $new->end_day = $value['end_day'];
            $new->activities = $value['activities'];
            $new->save();
        }
    }
}