<?php

namespace App\Repository;

use App\Repository\ToolRepository;
use App\Repository\CroquiRepository;
use App\Result;

class ResultRepository {

    public function result($data)
    {
        $c = new CroquiRepository;
        $croqui = $c->result($data);
        $t = new ToolRepository;
        $tools = $t->get($data['solicitation_id']);
        $result = array();
        $result['tools'] = $tools;
        $result['afs'] = $c->getAfs($data);
        foreach ($croqui->all() as $key => $value) {
            $dt = [];
            foreach ($tools->all() as $k => $v) {
                $data = [];
                $id = strval($v->id);
                $data['id'] = $id;
                $data['result'] = $this->getResult($value->id, $v->id);
                $dt[] = $data;
            }
            $value['tools'] = $dt;
            $result['data'][] = $value;
        }

        return $result;
    }

    public function getResult($croqui, $tool)
    {
        $result = Result::where('croqui_id', $croqui)
            ->where('tool_id', $tool)
            ->first();
        if(is_null($result)) {
            return $result;
        } else {
            return $result->result;
        }
    }
}