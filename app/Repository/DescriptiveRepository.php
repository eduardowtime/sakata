<?php

namespace App\Repository;

use App\Descriptive;

class DescriptiveRepository
{
    /**
     * Atualização de status da solicitação
     * 
     * @param int $solicitationId 
     * @param int $statusId 
     * 
     * @return static null 
     */
    public function countSchedules($id) 
    {
        return \DB::table('PROCEDURE_SCHEDULE')->where('procedure_id', $id)->count();
    }

    public function clone($procedure, $newProcedure)
    {
        $descriptive = Descriptive::where('procedure_id', $procedure)->first();
        $new = new Descriptive;
        $new->procedure_id = $newProcedure;
        $new->descriptive = $descriptive['descriptive'];
        $new->save();
        return $new;
    }
}