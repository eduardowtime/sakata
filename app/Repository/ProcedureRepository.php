<?php

namespace App\Repository;

use App\Procedure;
use App\Repository\ActivityRepository;
use App\Repository\DescriptiveRepository;

class ProcedureRepository
{
    /**
     * Atualização de status da solicitação
     * 
     * @param int $solicitationId 
     * @param int $statusId 
     * 
     * @return static null 
     */
    public function countSchedules($id) 
    {
        return \DB::table('PROCEDURE_SCHEDULE')->where('procedure_id', $id)->count();
    }

    public function clone($protocol, $newProtocol)
    {
        $procedure = Procedure::where('protocol_id', $protocol)->get();
        foreach ($procedure->all() as $key => $value) {
            $new = new Procedure;
            $new->protocol_id = $newProtocol;
            $new->subprogram_id = $value['subprogram_id'];
            $new->subprogram = $value['subprogram'];
            $new->resistance = $value['resistance'];
            $new->methodology = $value['methodology'];
            $new->description = $value['description'];
            $new->save();

            $activities = new ActivityRepository;
            $activities->clone($value['id'], $new->id);

            $descriptive = new DescriptiveRepository;
            $descriptive->clone($value['id'], $new->id);
        }
    }

    public function listProcedureId($protocol_id) {
        $procedure = Procedure::select('id')->where('protocol_id', $protocol_id)->get();
        $plucked = $procedure->pluck('id');
        return $plucked;
    }

    public function getProcedureId($protocol_id, $method) {
        $procedure = Procedure::select('id')
            ->where('protocol_id', $protocol_id)
            ->where('methodology', $method)
            ->first();
        if($procedure)
            return $procedure->id;
        else 
            return null;
    }
}