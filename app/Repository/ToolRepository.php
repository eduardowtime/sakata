<?php

namespace App\Repository;

use App\Tool;

/**
 * 
 */
class ToolRepository
{
    
    public function addTools($solicitation_id, $tools)
    {
        foreach ($tools as $key => $value) {
            \DB::table('tools_solicitation')->insert(
                [
                    'solicitation_id' => $solicitation_id, 
                    'tool_id' => $value['id']
                ]
            );
        }
    }

    public function get($solicitation_id)
    {
        return \DB::table('tools_solicitation')
            ->select('tools.id', 'tools.name')
            ->join('tools', 'tools.id', 'tools_solicitation.tool_id')
            ->where('solicitation_id', $solicitation_id)
            ->get();
    }
}