<?php

namespace App\Repository;

use App\Schedule;

class ScheduleRepository
{
    /**
     * Atualização de status da solicitação
     * 
     * @param int $solicitationId 
     * @param int $statusId 
     * 
     * @return static null 
     */
    public function count($id) 
    {
        return $schedule = Schedule::where('solicitation_id', $id)->count();

    }
}