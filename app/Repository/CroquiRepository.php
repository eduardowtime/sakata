<?php

namespace App\Repository;

use App\Croqui;
use App\Repository\SolicitationRepository;

class CroquiRepository
{

    public function result($data)
    {
        $result = Croqui::select(
                'croqui.id',
                'croqui.af',
                'croqui.index'
            )
            ->where('solicitation_id', $data['solicitation_id'])
            ->orderBy('af')
            ->orderBy('index')
            ->get();

        return $result;
    }

    public function getAfs($data)
    {
        $result = Croqui::select('croqui.af')
            ->where('solicitation_id', $data['solicitation_id'])
            ->orderBy('af')
            ->distinct()
            ->get();

        return $result;
    }

    public function getFields() 
    {
        $fields = array(
            'A1', 'B1', 'C1', 'D1', 'E1', 'F1', 'G1', 'H1',
            'A2', 'B2', 'C2', 'D2', 'E2', 'F2', 'G2', 'H2',
            'A3', 'B3', 'C3', 'D3', 'E3', 'F3', 'G3', 'H3',
            'A4', 'B4', 'C4', 'D4', 'E4', 'F4', 'G4', 'H4',
            'A5', 'B5', 'C5', 'D5', 'E5', 'F5', 'G5', 'H5',
            'A6', 'B6', 'C6', 'D6', 'E6', 'F6', 'G6', 'H6',
            'A7', 'B7', 'C7', 'D7', 'E7', 'F7', 'G7', 'H7',
            'A8', 'B8', 'C8', 'D8', 'E8', 'F8', 'G8', 'H8',
            'A9', 'B9', 'C9', 'D9', 'E9', 'F9', 'G9', 'H9',
            'A10', 'B10', 'C10', 'D10', 'E10', 'F10', 'G10', 'H10',
            'A11', 'B11', 'C11', 'D11', 'E11', 'F11', 'G11', 'H11',
            'A12', 'B12', 'C12', 'D12', 'E12', 'F12', 'G12', 'H12',
        );
        return $fields;
    }

    public function setIndex($af, $solicitation_id) {
        $croqui = Croqui::select('index')
            ->where('af', $af)
            ->where('solicitation_id', $solicitation_id)
            ->orderBy('index', 'desc')
            ->first();
        if(is_null($croqui)) {
            return 1;
        } else {
            return $croqui->index+1;
        }
    }

    public function getBoard($solicitation_id, $board=null)
    {
        $data = Croqui::where('solicitation_id', $solicitation_id);
        if(!is_null($board)) {
            $data->where('board', $board);
        }
        $data = $data->orderBy('id')->get();
        $res = array();
        foreach ($data->all() as $key => $value) {
            $res[$value['field']] = $value;
        }
        return $res;
    }

    public function itemsValue($solicitation_id)
    {
        $croqui = Croqui::select('af', \DB::raw('count(*) as total'))
            ->where('solicitation_id', $solicitation_id)
            ->groupBy('af')
            ->get();
        return $croqui;
    }

    public function setFields($solicitation_id, $board)
    {
        $fields = $this->getFields();
        $data = Croqui::select('field')
            ->where('solicitation_id', $solicitation_id)
            ->where('board', $board)
            ->get();
        foreach ($data->all() as $key => $value) {
            unset($fields[array_search($value['field'], $fields)]);
        }
        $arr = array();
        foreach ($fields as $key => $value) {
            $arr[] = $value;
        }
        return $arr;
    }

    public function getNumBoards($solicitation_id)
    {
        $data = Croqui::select('board')
            ->where('solicitation_id', $solicitation_id)
            ->max('board');
        
        if(is_null($data)) {
            return 0;
        }
        return $data;
    }

    public function verifyRecord($solicitation_id) 
    {
        $qtd = \DB::table('items_solicitations')
            ->where('solicitations_id', $solicitation_id)
            ->where('active', 1)
            ->sum('plants');
        $croqui = Croqui::where('solicitation_id', $solicitation_id)->count();
        if($qtd != $croqui) {
            return 0;
        }
        return 1;
    }

}