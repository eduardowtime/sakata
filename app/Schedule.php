<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Schedule extends Model
{
    protected $table = 'schedule_dates';

    public function setAccomplishedDateAttribute($value) {
        $this->attributes['accomplished_date'] = \Carbon\Carbon::parse($value);
    }

    public function setProposedDateAttribute($value) {
        $this->attributes['proposed_date'] = \Carbon\Carbon::parse($value);
    }

    public function getProposedDateAttribute($value) {
        if($value != null)
            return \Carbon\Carbon::parse($value)->format('Y-m-d');
        else
            return $value;
    }

    public function getAccomplishedDateAttribute($value) {
        if($value != null)
            return \Carbon\Carbon::parse($value)->format('Y-m-d');
        else
            return $value;
    }
}
