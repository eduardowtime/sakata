<?php

use Illuminate\Http\Request;

use Laravel\Passport\Passport;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Passport::routes();

Route::get('/attachments/download', 'AttachmentController@download');
Route::group([
    'middleware' => ['auth:api', 'api']
], function () {
    Route::get('levels', 'LevelController@index');
    Route::post('mail', 'ViewController@mail');
    Route::get('status', 'StatusController@index');
    Route::get('/user', 'UserController@loggedUser');
    Route::get('/entry/sectors/{trial}/{ano}', 'EntryController@getSectors');
    Route::get('/entry/subprogram/{trial}/{ano}/{setor}', 'EntryController@getSubProgram');
    Route::get('/entry/project/{trial}/{ano}/{setor}/{subp}', 'EntryController@getProject');
    Route::get('/view/project', 'ViewController@showProjects');
    Route::get('/view/project/{id}', 'ViewController@findProjects');
    Route::get('/view/subprogram', 'ViewController@showSubPrograms');
    Route::get('/view/subprogram/{id}', 'ViewController@findSubPrograms');
    Route::get('/view/sector', 'ViewController@showSectors');
    Route::get('/descriptive/bypro/{id}', 'DescriptiveController@getByProcedure');
    Route::get('/procedures/bypro/{id}', 'ProcedureController@showByProtocol');
    Route::get('/solicitations/{id}/protocol', 'SolicitationController@allProtocol');
    Route::get('/solicitations/{id}/report', 'SolicitationController@report');
    Route::get('/solicitations/find', 'SolicitationController@find');
    Route::get('/croqui/range', 'CroquiController@range');
    Route::post('/report/solicitations', 'ReportController@solicitations');
    Route::post('/entry/', 'EntryController@setAf');
    Route::get('/items/solicitations/{id}', 'ItemController@getBySolicitation');
    Route::post('/protocols/{id}/clone', 'ProtocolController@clone');
    Route::post('/items/record', 'ItemController@record');
    Route::post('/solicitations/{id}/status', 'SolicitationController@setStatus');
    Route::post('/solicitations/report', 'SolicitationController@saveReport');
    Route::post('/solicitations/record', 'SolicitationController@record');
    Route::post('/croqui/reset', 'CroquiController@reset');
    Route::post('/result/{id}', 'ResultController@save');
    Route::post('/user/{id}/password', 'UserController@updatePassword');
    Route::apiResources([
            'departments' => 'DepartmentController',
            'laboratories' => 'LaboratoryController',
            'tools' => 'ToolController',
            'sectors' => 'SectorController',
            'users' => 'UserController',
            'solicitations' => 'SolicitationController',
            'items' => 'ItemController',
            'protocols' => 'ProtocolController',
            'procedures' => 'ProcedureController',
            'activities' => 'ActivityController',
            'descriptive' => 'DescriptiveController',
            'schedules' => 'ScheduleController',
            'attachments' => 'AttachmentController',
            'croqui' => 'CroquiController',
            'result' => 'ResultController',
            'permission' => 'PermissionController'
    ]);
});
